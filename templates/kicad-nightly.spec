%global snapdate @SNAPSHOTDATE@
%global commit0 @COMMITHASH0@
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global docrev @DOCREV@
%global symrev @SYMREV@
%global footrev @FOOTREV@
%global p3drev @P3DREV@
%global tplrev @TPLREV@

%global kicad_prefix %{_libdir}/kicad-nightly

# We need to strip the leading "/usr/" from the python3_sitearch macro,
# and use that as the location of our python files.
%global kicad_python_path %{lua:
    -- Anchor the match to the beginning of the string.
    --
    -- We also want to swallow the path separator so we wind up with
    -- a relative path.
    local prefix = "^" .. rpm.expand("%{_prefix}") .. "/"
    local python3_sitearch = rpm.expand("%{python3_sitearch}")
    local result = string.gsub(python3_sitearch, prefix, "")
    print(result)
}

Name:           kicad-nightly
Version:        @VERSION@
Release:        1.%{snapdate}git%{shortcommit0}%{?dist}
Epoch:          1
Summary:        EDA software suite for creation of schematic diagrams and PCBs
License:        GPL-3.0-or-later
URL:            https://www.kicad.org

Source0:        https://gitlab.com/kicad/code/kicad/-/archive/%{commit0}/kicad-%{commit0}.tar.gz
Source1:        https://gitlab.com/kicad/services/kicad-doc/-/archive/%{docrev}/kicad-doc-%{docrev}.tar.gz
Source2:        https://gitlab.com/kicad/libraries/kicad-templates/-/archive/%{tplrev}/kicad-templates-%{tplrev}.tar.gz
Source3:        https://gitlab.com/kicad/libraries/kicad-symbols/-/archive/%{symrev}/kicad-symbols-%{symrev}.tar.gz
Source4:        https://gitlab.com/kicad/libraries/kicad-footprints/-/archive/%{footrev}/kicad-footprints-%{footrev}.tar.gz
Source5:        https://gitlab.com/kicad/libraries/kicad-packages3D/-/archive/%{p3drev}/kicad-packages3D-%{p3drev}.tar.gz

# https://gitlab.com/kicad/code/kicad/-/issues/237
ExclusiveArch:  x86_64 aarch64 ppc64le

BuildRequires:  boost-devel
BuildRequires:  cmake
BuildRequires:  desktop-file-utils
BuildRequires:  gcc-c++
BuildRequires:  gettext
BuildRequires:  glew-devel
BuildRequires:  glm-devel
BuildRequires:  gtk3-devel
BuildRequires:  libappstream-glib
BuildRequires:  libcurl-devel
BuildRequires:  libgit2-devel
BuildRequires:  libngspice-devel
BuildRequires:  libsecret-devel
BuildRequires:  libzstd-devel
BuildRequires:  make
BuildRequires:  nng-devel
BuildRequires:  opencascade-devel
BuildRequires:  protobuf-compiler
BuildRequires:  protobuf-devel
BuildRequires:  python3-devel
BuildRequires:  python3-wxpython4
BuildRequires:  shared-mime-info
BuildRequires:  swig
BuildRequires:  unixODBC-devel
BuildRequires:  wxGTK-devel
BuildRequires:  zlib-devel

# Documentation
BuildRequires:  po4a
BuildRequires:  rubygem-asciidoctor

Provides:       bundled(fmt) = 9.0.0
Provides:       bundled(libdxflib) = 3.26.4
Provides:       bundled(polyclipping) = 6.4.2
Provides:       bundled(potrace) = 1.15

Requires:       electronics-menu
Requires:       libgit2
Requires:       libngspice
Requires:       libsecret
Requires:       ngspice-codemodel
Requires:       protobuf
Requires:       python3-wxpython4
Requires:       unixODBC

Suggests:       kicad

%description
KiCad is EDA software to design electronic schematic
diagrams and printed circuit board artwork of up to
32 layers.

%package        packages3d
Summary:        3D Models for KiCad
License:        CC-BY-SA
BuildArch:      noarch
Requires:       kicad-nightly >= 9.0.0

%description    packages3d
3D Models for KiCad.

%package        doc
Summary:        Documentation for KiCad
License:        GPL-3.0-or-later or CC-BY
BuildArch:      noarch

%description    doc
Documentation for KiCad.


%prep
%setup -q -n kicad-%{commit0} -a 1 -a 2 -a 3 -a 4 -a 5


%build

# KiCad application
%cmake \
    -DKICAD_IPC_API=ON \
    -DKICAD_SCRIPTING_WXPYTHON=ON \
    -DKICAD_USE_OCC=ON \
    -DKICAD_INSTALL_DEMOS=ON \
    -DKICAD_BUILD_QA_TESTS=OFF \
    -DKICAD_BUILD_I18N=ON \
    -DKICAD_I18N_UNIX_STRICT_PATH=ON \
    -DKICAD_USE_EGL=OFF \
    -DKICAD_USE_CMAKE_FINDPROTOBUF=ON \
    -DKICAD_VERSION_EXTRA=%{release} \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo \
    -DPYTHON_SITE_PACKAGE_PATH=%{kicad_python_path} \
    -DCMAKE_INSTALL_PREFIX=%{kicad_prefix} \
    -DCMAKE_INSTALL_DATADIR=%{_datadir} \
    -DCMAKE_INSTALL_DOCDIR=%{_docdir} \
    -DDEFAULT_INSTALL_PATH=%{kicad_prefix} \
    -DKICAD_DATA=%{_datadir}/%{name} \
    -DKICAD_DOCS=%{_docdir}/%{name}
%cmake_build

# Templates
pushd kicad-templates-%{tplrev}/
%cmake \
    -DKICAD_DATA=%{_datadir}/%{name}
%cmake_build
popd

# Symbol libraries
pushd kicad-symbols-%{symrev}/
%cmake \
    -DKICAD_DATA=%{_datadir}/%{name}
%cmake_build
popd

# Footprint libraries
pushd kicad-footprints-%{footrev}/
%cmake \
    -DKICAD_DATA=%{_datadir}/%{name}
%cmake_build
popd

# 3D models
pushd kicad-packages3D-%{p3drev}/
%cmake \
    -DKICAD_DATA=%{_datadir}/%{name}
%cmake_build
popd

# Documentation (HTML only)
pushd kicad-doc-%{docrev}/
%cmake \
    -DKICAD_DOC_PATH=%{_docdir}/kicad-nightly/help \
    -DPDF_GENERATOR=none \
    -DBUILD_FORMATS=html
%cmake_build
popd


%install

# KiCad application
%cmake_install
mkdir -p %{buildroot}%{_docdir}/%{name}/
cp -p AUTHORS.txt %{buildroot}%{_docdir}/%{name}/

# Python scripts in non-standard paths require manual byte compilation
%py_byte_compile %{python3} %{buildroot}%{kicad_prefix}/%{_lib}/python%{python3_version}/site-packages/

# Wrapper scripts
mkdir -p %{buildroot}%{_bindir}
ls -1 %{buildroot}%{kicad_prefix}/bin/ | grep -v -F '.kiface' | \
    while read application; do
        (
            echo '#!/usr/bin/sh'
            echo ''
            echo 'export LD_LIBRARY_PATH=%{kicad_prefix}/%{_lib}/'
            echo ''
            echo "%{kicad_prefix}/bin/${application} \"\$@\""
        ) > %{buildroot}%{_bindir}/${application}-nightly
    done

# Icons
pushd resources/linux/icons-nightly/hicolor/
ls -1 | \
    while read size; do
        cp -p ${size}/apps/* %{buildroot}%{_datadir}/icons/hicolor/${size}/apps/
    done
popd
pushd %{buildroot}%{_datadir}/icons/hicolor/
ls -1 | \
    while read size; do
        ls -1 ${size}/apps/ | \
            while read icon; do
                mv ${size}/apps/${icon} ${size}/apps/${icon%%.*}-nightly.${icon##*.}
            done
        ls -1 ${size}/mimetypes/ | grep 'kicad' | \
            while read icon; do
                mv ${size}/mimetypes/${icon} ${size}/mimetypes/${icon%%%%kicad*}kicad-nightly${icon#*kicad}
            done
    done
popd

# MIME files
pushd %{buildroot}%{_datadir}/mime/packages/
sed -i \
    -e 's/x-kicad/x-kicad-nightly/g' \
    -e 's/KiCad/KiCad Nightly/g' \
    -e 's/weight="[0-9]*"/weight="0"/g' \
    -e 's/priority="[0-9]*"/priority="0"/g' \
    kicad-kicad.xml
ls -1 | grep -F '.xml' | \
    while read mimefile; do
        mv ${mimefile} ${mimefile%%%%-*}-nightly-${mimefile#*-}
    done
popd

# Application launchers
pushd %{buildroot}%{_datadir}/applications/
ls -1 | grep -F '.desktop' | \
    while read launcher; do
        sed -i \
            -e 's/^Name\(.*\)=\([^(]*\)$/Name\1=\2 NIGHTLY/g' \
            -e 's/^Name\(.*\)=\(.*\)\( (.*\)$/Name\1=\2 NIGHTLY\3/g' \
            -e 's/^Icon=\(.*\)$/Icon=\1-nightly/g' \
            -e 's/^Exec=\([^ ]*\)\(.*\)$/Exec=\1-nightly\2/g' \
            -e 's/^StartupWMClass=\([^ ]*\)\(.*\)$/StartupWMClass=\1-nightly\2/g' \
            -e 's/^MimeType=\(.*kicad\)\(.*;\)$/MimeType=\1\2\1-nightly\2/g' \
            ${launcher}
        launcher_new=${launcher%%.*}-nightly.desktop
        launcher_new=zzz.${launcher_new:4}
        mv ${launcher} ${launcher_new}
        desktop-file-install \
            --dir %{buildroot}%{_datadir}/applications/ \
            --remove-category Science \
            --delete-original \
            ${launcher_new}
    done
popd

# AppStream metainfo file
pushd %{buildroot}%{_metainfodir}
sed -i \
    -e 's/\(<id>.\+\)\(<\/id>\)$/\1_nightly\2/g' \
    -e 's/\(<name.*>.\+\)\(<\/name>\)$/\1 Nightly\2/g' \
    -e 's/\(<launchable.*>\).\+\(<\/launchable>\)$/\1zzz.kicad.kicad-nightly.desktop\2/g' \
    -e 's/\(<binary>.\+\)\(<\/binary>\)$/\1-nightly\2/g' \
    -e 's/x-kicad/x-kicad-nightly/g' \
    org.kicad.kicad.metainfo.xml
mv org.kicad.kicad.metainfo.xml org.kicad.kicad_nightly.metainfo.xml
popd

# Templates
pushd kicad-templates-%{tplrev}/
%cmake_install
cp -p LICENSE.md ../LICENSE-templates.md
popd

# Symbol libraries
pushd kicad-symbols-%{symrev}/
%cmake_install
cp -p LICENSE.md ../LICENSE-symbols.md
popd

# Footprint libraries
pushd kicad-footprints-%{footrev}/
%cmake_install
cp -p LICENSE.md ../LICENSE-footprints.md
popd

# 3D models
pushd kicad-packages3D-%{p3drev}/
%cmake_install
popd

# Documentation
pushd kicad-doc-%{docrev}/
%cmake_install
popd

# Note: We don't want to use the find_lang macro here, because we are
# including everything within the kicad_prefix.  Basically, all our .mo
# files will wind up in the rpm result automatically.

%check

appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/*.metainfo.xml


%files
%doc AUTHORS.txt
%attr(0755, root, root) %{_bindir}/*
%{kicad_prefix}/
%{_datadir}/%{name}/
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/apps/*.*
%{_datadir}/icons/hicolor/*/mimetypes/application-x-*.*
%{_datadir}/mime/packages/*.xml
%{_metainfodir}/*.metainfo.xml
%license LICENSE*
%exclude %{_datadir}/%{name}/3dmodels/*

%files packages3d
%{_datadir}/%{name}/3dmodels/*.3dshapes
%license kicad-packages3D-%{p3drev}/LICENSE*

%files doc
%{_docdir}/%{name}/help/
%exclude %{_docdir}/%{name}/AUTHORS.txt
%license kicad-doc-%{docrev}/LICENSE*


%changelog
